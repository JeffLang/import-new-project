import React, { createContext, useState, Context as ContextProps } from 'react'
// 统一导出一个context对象
// export const Context: ContextProps<any> = createContext({})
export const Context: ContextProps<any> = createContext({})
// 创建一个函数组件
const StoreContext: React.FC = ({ children }) => {
  // 使用userState让函数组件拥有状态state
  // 第一个参数是state中的数据，第二个是更新state的数据
  const [userinfo, setUSerInfo] = useState<any>({ name: 'jeff', age: 18, gender: 'sex' })
  return (
    <Context.Provider value={{ userinfo, setUSerInfo }}>
      <p>{children}</p>
    </Context.Provider>
  )
}
// 导出函数组件
export default StoreContext
